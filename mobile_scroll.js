/*
//SK 2014-09-09
IEMobile specific scroll functions
Uses jQuery and JavaScript

src:   
* WP7 http://blogs.msdn.com/b/iemobile/archive/2010/12/08/targeting-mobile-optimized-css-at-windows-phone-7.aspx
* http://stackoverflow.com/questions/2400935/browser-detection-in-javascript
* NOT http://stackoverflow.com/questions/2490452/using-javascript-to-detect-browser-type
* USED!! http://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-handheld-device-in-jquery
*/
  
navigator.sayswho= (function(){
    var ua= navigator.userAgent, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\bOPR\/(\d+)/)
        if(tem!= null) return 'Opera '+tem[1];
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

document.write ("## mobile_scroll.js ");
document.write ( navigator.sayswho );
document.write (" - user agent match  ");
document.write(navigator.userAgent.match(/IEMobile/i));


  if (navigator.userAgent.match(/IEMobile/i) == "IEMobile") {
      document.write ( " I AM A WINDOWS MOBILE!! " );
      
    function scrollBody(scrollToTarget, duration) {
        var getIE = navigator.sayswho;
        if (getIE.indexOf("10") >= 0) {
        //for MSIE 10: 
          window.scrollTo(0,scrollToTarget); // first value for left offset, second value for top offset
        } else {
        //for other IEMobile versions
          $('html').scrollTop(scrollTo);
        } 
    }

  } else { 
      document.write ( " Not a windows mobile " );

    function scrollBody(scrollToTarget, duration) {
      $('html, body').animate({ scrollTop: scrollToTarget }, duration);
    }
        
  }
    
