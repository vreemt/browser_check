# Browser check

See index.html

* Uses pure JS to get browser info
* Has mobile scrolling functions based on that browser info
* Contains further reading (see also Resources)

## Resources

* Fiddle: <http://jsfiddle.net/vreemt/jk36nt2f/>
* Src: <http://stackoverflow.com/questions/2400935/browser-detection-in-javascript>
* original: <http://jsfiddle.net/ZXFEk/> (alert)
* See also <http://www.quirksmode.org/js/detect.html>
* See also <http://whichbrowser.net>
    
    
